//
// Created by Tobias on 1/26/2021.
//

#include "legacy_types.hpp"

#include <nlohmann/json.hpp>

namespace npp::legacy {

void Line::push(PointData pd) {
	m_points.push_back(pd);
}
const Line::container_t &Line::points() const {
	return m_points;
}
Line::container_t &Line::points() {
	return m_points;
}
Line::cit_t Line::begin() const {
	return m_points.cbegin();
}
Line::cit_t Line::end() const {
	return m_points.cend();
}
Line::it_t Line::begin() {
	return m_points.begin();
}
Line::it_t Line::end() {
	return m_points.end();
}

using nlohmann::json;

void to_json(json &j, const PointData &pd) {
	j = nlohmann::json{{"valid", pd.valid},
					   {"x", pd.x},
					   {"y", pd.y},
					   {"pressure", pd.pressure}};
}

void from_json(const json &j, PointData &pd) {
	j.at("valid").get_to(pd.valid);
	j.at("x").get_to(pd.x);
	j.at("y").get_to(pd.y);
	j.at("pressure").get_to(pd.pressure);
}

void to_json(json &j, const Line &line) {
	j = json{{"points", line.points()}};
}

void from_json(const json &j, Line &line) {
	j.at("points").get_to(line.points());
}

} // namespace npp::legacy
