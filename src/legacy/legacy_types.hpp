//
// Created by Tobias on 1/26/2021.
//

#ifndef NPP_LEGACY_TYPES_HPP
#define NPP_LEGACY_TYPES_HPP

#include <nlohmann/json_fwd.hpp>

namespace npp::legacy {

struct PointData {
	bool valid{false};
	float x{};
	float y{};
	float pressure{};
};

class Line {
public:
	using container_t = std::vector<PointData>;
	using cit_t = container_t::const_iterator;
	using it_t = container_t::iterator;

private:
	container_t m_points;

public:
	Line() = default;
	void push(PointData pd);
	[[nodiscard]] const container_t &points() const;
	container_t &points();

	[[nodiscard]] cit_t begin() const;
	[[nodiscard]] it_t begin();
	[[nodiscard]] cit_t end() const;
	[[nodiscard]] it_t end();
};

void to_json(nlohmann::json &j, const PointData &pd);
void from_json(const nlohmann::json &j, PointData &pd);

void to_json(nlohmann::json &j, const Line &pd);
void from_json(const nlohmann::json &j, Line &pd);

} // namespace npp::legacy

#endif // NPP_LEGACY_TYPES_HPP
