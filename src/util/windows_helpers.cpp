//
// Created by Tobias on 1/26/2021.
//

#include "windows_helpers.hpp"

#include <filesystem>

#include <Shlobj.h>

static const std::string &get_documents_path();

static bool dir_exists(const std::string &path) {
	DWORD ftyp = GetFileAttributesA(path.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES) {
		return false; // something is wrong with your path!
	}

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY) {
		return true; // this is a directory!
	}

	return false; // this is not a directory!
}

namespace npp::win {

const std::string &get_default_path() {
	static bool initialized{};
	static std::string str{};
	if (!initialized) { str = get_documents_path() + "/npp/"; }
	return str;
}

void prepare_folders() {
	auto path{get_documents_path() + "/npp/"};
	if (!dir_exists(path)) {
		// Create folder
		std::filesystem::create_directories(path);
	}
}

HCURSOR cursor() {
	static HCURSOR c = LoadCursor(nullptr, IDC_CROSS);
	return c;
}

} // namespace npp::win

static const std::string &get_documents_path() {
	static bool initialized{};
	static std::string str{};
	if (!initialized) {
		PWSTR documentsPath = nullptr;
		HRESULT hr = SHGetKnownFolderPath(FOLDERID_Documents, 0, nullptr,
										  &documentsPath);
		if (FAILED(hr)) {
			CoTaskMemFree(documentsPath);
			str = "C:/";
		} else {
			int cNum = WideCharToMultiByte(CP_UTF8, 0, documentsPath, -1,
										   nullptr, 0, nullptr, nullptr);
			auto *cStr = new char[cNum];
			WideCharToMultiByte(CP_UTF8, 0, documentsPath, -1, cStr, cNum,
								nullptr, nullptr);
			str.assign(cStr);
			delete[] cStr;
		}
	}
	return str;
}