//
// Created by Tobias on 1/26/2021.
//

#ifndef NPP_WINDOWS_HELPERS_HPP
#define NPP_WINDOWS_HELPERS_HPP

#include <string>

#include "../aggregates/win32.hpp"

namespace npp::win {

const std::string &get_default_path();
void prepare_folders();

HCURSOR cursor();

} // namespace npp::win

#endif // NPP_WINDOWS_HELPERS_HPP
