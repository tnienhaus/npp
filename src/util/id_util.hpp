//
// Created by Tobias on 1/22/2021.
//

#ifndef NPP_ID_UTIL_HPP
#define NPP_ID_UTIL_HPP

namespace npp::id_util {

using id_t = unsigned long long int;

constexpr id_t invalid_id() {
	return std::numeric_limits<id_t>::max();
}

template<id_t = 0>
id_t new_id() {
	static id_t id{};
	id++;
	if (id >= invalid_id()) { id = 0; }
	return id;
}

} // namespace npp::id_util

#endif // NPP_ID_UTIL_HPP
