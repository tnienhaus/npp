//
// Created by Tobias on 1/11/2021.
//

#ifndef NPP_WIN32_HELPERS_HPP
#define NPP_WIN32_HELPERS_HPP

#include <concepts>

#include "../aggregates/d2d.hpp"
#include "../aggregates/win32.hpp"

namespace npp::util {

template<typename T>
void com_safe_release(T **comObj) requires std::is_base_of_v<IUnknown, T> {
	if (*comObj) {
		(*comObj)->Release();
		*comObj = nullptr;
	}
}

template<typename T>
void com_safe_release(
	CComPtr<T> &comPtr) requires std::is_base_of_v<IUnknown, T> {
	if (comPtr) {
		comPtr.Release();
		comPtr = nullptr;
	}
}

} // namespace npp::util

#endif // NPP_WIN32_HELPERS_HPP
