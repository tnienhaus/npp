//
// Created by Tobias on 1/22/2021.
//

#include "ApplicationContext.hpp"
#include <imgui.h>

#ifdef DEBUG
static void error_description(HRESULT hr) {
	if (FACILITY_WINDOWS == HRESULT_FACILITY(hr)) hr = HRESULT_CODE(hr);
	TCHAR *szErrMsg;

	if (FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL,
			hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&szErrMsg, 0,
			nullptr) != 0) {
		_tprintf(TEXT("%s"), szErrMsg);
		LocalFree(szErrMsg);
	} else
		_tprintf(TEXT("[Could not find a description for error # %#x.]\n"), hr);
}
#else
static void error_description(HRESULT) {}
#endif

namespace npp {

ApplicationContext::ApplicationContext(PCWSTR lpWindowName, DWORD dwStyle) :
	m_drawCallbacks{},
	m_windowHandler{std::make_unique<graphics::WindowHandler>(*this)},
	m_uiHandler{nullptr},
	m_renderer{nullptr},
	m_tablet{nullptr},
	m_file{std::make_unique<file::File>(*this)},
	m_fileDialog{*this},
	m_shouldStartNewLine{false},
	m_wantsToClose{false},
	m_fullRedrawThisFrame{false} {
	m_windowHandler->create(lpWindowName, dwStyle);

	m_uiHandler = std::make_unique<ui::UiHandler>(
		*this, m_windowHandler->get_window(), m_windowHandler->d3d_device(),
		m_windowHandler->d3d_render_target());
	m_renderer =
		std::make_unique<graphics::D2DBitmapRenderer<graphics::TabletRenderer>>(
			*this);
	m_renderer->set_render_target(m_windowHandler->d2d_render_target());
	auto hr = m_renderer->initialize();
	if (FAILED(hr)) { error_description(hr); }

	m_tablet = std::move(std::make_unique<Tablet>(*this));
}

LRESULT ApplicationContext::handle_events(UINT msg, WPARAM wp, LPARAM lp) {
	if (ui::UiHandler::handle_message(m_windowHandler->get_window(), msg, wp,
									  lp)) {
		return 0;
	}

	if (handle_tablet_event(m_tablet->handle_event(msg, wp))) { return 0; }

	switch (msg) {
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	case WM_CLOSE:
		if (m_file->has_unsaved_changes()) {
			m_wantsToClose = true;
		} else {
			DestroyWindow(m_windowHandler->get_window());
		}
		return 0;
	case WM_SIZE: {
		auto w{(UINT)LOWORD(lp)};
		auto h{(UINT)HIWORD(lp)};
		m_windowHandler->on_resize(w, h);
		m_uiHandler->set_render_target(m_windowHandler->d3d_render_target());
		m_renderer->set_render_target(m_windowHandler->d2d_render_target());
		m_renderer->on_resize(w, h);
		m_fullRedrawThisFrame = true;
		return 0;
	}
	case WM_PAINT: {
		auto oldState{m_fullRedrawThisFrame};

		m_windowHandler->clear();
		if (!m_renderer->draw()) {
			if (SUCCEEDED(m_windowHandler->create_d2d_render_target())) {
				m_renderer->set_render_target(
					m_windowHandler->d2d_render_target());
			}
		}

		{
			ui::UiHandler::Frame f{};
			// after ImGui ui stuff -> set mouse cursor, etc.
			set_imgui_mouse_pos();
			m_uiHandler->frame();
			m_fileDialog.display();
		}
		m_windowHandler->draw();
		if (wants_full_redraw_this_frame()) { std::cout << "Full redraw!!!\n"; }
		if (oldState && m_fullRedrawThisFrame) {
			m_fullRedrawThisFrame = false;
		}
		return 0;
	}
	default:
		return DefWindowProc(m_windowHandler->get_window(), msg, wp, lp);
	}
}

void ApplicationContext::show(int cmdShow) {
	m_windowHandler->show(cmdShow);
}

HWND ApplicationContext::get_window() const {
	return m_windowHandler->get_window();
}

Tablet *ApplicationContext::get_tablet() {
	return m_tablet.get();
}

void ApplicationContext::set_imgui_mouse_pos() {
	auto pos = m_tablet->get_pen_pos();
	if (pos.has_value()) {
		auto &io = ImGui::GetIO();
		io.MousePos = {pos->x, pos->y};
	}
}

id_util::id_t ApplicationContext::register_draw_callback(
	const ApplicationContext::draw_callback_t &cb) {
	auto id{id_util::new_id<s_callbackIdNum>()};
	m_drawCallbacks.emplace(id, cb);
	return id;
}

bool ApplicationContext::deregister_draw_callback(id_util::id_t id) {
	return m_drawCallbacks.erase(id) > 0;
}

bool ApplicationContext::handle_tablet_event(Tablet::Event event) {
	if (event != Tablet::Event::UNHANDLED) {
		if (event == Tablet::Event::DOWN) { m_shouldStartNewLine = true; }
		if (m_tablet->get_next_point_count() > 0) {
			std::vector<file::DataEntry> data;
			data.reserve(m_tablet->get_next_point_count());
			for (auto points = m_tablet->get_all(); const auto &p : points) {
				if (p.valid) {
					data.emplace_back(
						p.x, p.y,
						p.pressure * 2.5f); // TODO add size multiplier
				}
			}

			if (!data.empty()) {
				for (const auto &[_, cb] : m_drawCallbacks) {
					cb(m_shouldStartNewLine, data);
				}
				m_shouldStartNewLine = false;
			}
		}
		return true;
	} else {
		return false;
	}
}

const file::File *ApplicationContext::get_file() const {
	return m_file.get();
}

file::File *ApplicationContext::get_file() {
	return m_file.get();
}

bool ApplicationContext::want_to_close() const {
	return m_wantsToClose;
}

void ApplicationContext::cancel_close() {
	m_wantsToClose = false;
}

ui::FileDialog &ApplicationContext::get_file_dialog() {
	return m_fileDialog;
}

void ApplicationContext::open_file(const std::filesystem::path &path) {
	m_file.reset();
	m_file = std::move(file::File::open(*this, path));
	if (!m_file) { std::cout << "Couldn't open file!\n"; }
	m_fullRedrawThisFrame = true;
}

void ApplicationContext::new_file() {
	m_file.reset();
	m_file = std::make_unique<file::File>(*this);
	m_fullRedrawThisFrame = true;
}

bool ApplicationContext::wants_full_redraw_this_frame() const {
	return m_fullRedrawThisFrame;
}

} // namespace npp
