//
// Created by Tobias on 1/26/2021.
//

#include <fstream>

#include <snappy.h>

#include <nlohmann/json.hpp>

#include "fileformat_helpers.hpp"

namespace npp::file {

static nlohmann::json binary_to_json(const std::filesystem::path &pathToBnpp) {
	using nlohmann::json;
	std::ifstream f(pathToBnpp, std::ios::in | std::ios::binary);
	if (f.is_open()) {
		f.unsetf(std::ios::skipws);

		// get its size:
		std::streampos fileSize;

		f.seekg(0, std::ios::end);
		fileSize = f.tellg();
		f.seekg(0, std::ios::beg);

		std::string str;
		str.reserve(fileSize);

		str.insert(str.begin(), std::istream_iterator<char>(f),
				   std::istream_iterator<char>());

		std::string o;

		snappy::Uncompress(str.data(), str.size(), &o);

		return json::from_bson(o);
	} else {
		return json::object();
	}
}

static nlohmann::json plain_to_json(const std::filesystem::path &pathToNpp) {
	using nlohmann::json;
	std::ifstream file{pathToNpp};
	if (file.is_open()) {
		std::string contents;
		file.seekg(0, std::ios::end);
		contents.resize(file.tellg());
		file.seekg(0, std::ios::beg);
		file.read(&contents[0], contents.size());
		file.close();
		return json::parse(contents);
	} else {
		return json::object();
	}
}

nlohmann::json file_to_json(const std::filesystem::path &path) {
	using nlohmann::json;
	if (path.extension() == L".bnpp") {
		return binary_to_json(path);
	} else if (path.extension() == L".npp") {
		return plain_to_json(path);
	}
	return json::object();
}

static bool json_to_plain_file(const nlohmann::json &j,
							   const std::filesystem::path &path) {
	using nlohmann::json;
	std::ofstream file{path, std::ios::trunc};
	if (file.is_open()) {
		file << j.dump();
		file.close();
		return true;
	} else {
		return false;
	}
}

static bool json_to_binary_file(const nlohmann::json &j,
								const std::filesystem::path &path) {
	using nlohmann::json;
	std::ofstream file{path.c_str(),
					   std::ios::trunc | std::ios::out | std::ios::binary};
	if (file.is_open()) {
		//		auto bson{json::to_bson(j)};
		//		std::string out{bson.begin(), bson.end()};
		std::string bson;
		json::to_bson(j, bson);
		std::string compressed;
		compressed.reserve(bson.size());

		snappy::Compress(bson.data(), bson.size(), &compressed);

		file.write(compressed.data(),
				   compressed.size() * sizeof(std::string::value_type));
		file.close();
		return true;
	} else {
		return false;
	}
}

bool json_to_file(const nlohmann::json &j, const std::filesystem::path &path) {
	if (path.extension() == L".bnpp") {
		return json_to_binary_file(j, path);
	} else if (path.extension() == L".npp") {
		return json_to_plain_file(j, path);
	}
	return false;
}

} // namespace npp::file
