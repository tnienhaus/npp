//
// Created by Tobias on 1/25/2021.
//

#ifndef NPP_FILE_HPP
#define NPP_FILE_HPP

#include <filesystem>
#include <memory>
#include <optional>
#include <vector>

#include <nlohmann/json_fwd.hpp>

#include "../ContextHolder.hpp"

#include "../npp_version_info.hpp"
#include "../util/id_util.hpp"

#include "../legacy/legacy_types.hpp"

namespace npp::file {

struct DataEntry {
	float x{};
	float y{};
	float thickness{};
	// TODO color, etc.

	constexpr DataEntry() : x{}, y{}, thickness{} {}
	constexpr DataEntry(float x, float y, float thickness) :
		x{x},
		y{y},
		thickness{thickness} {}
	explicit DataEntry(const legacy::PointData &);
};

void to_json(nlohmann::json &j, const DataEntry &de);
void from_json(const nlohmann::json &j, DataEntry &de);

using line_t = std::vector<DataEntry>;

class File : public ContextHolder {
private:
	using legacy_data_t = std::vector<legacy::Line>;

private:
	VersionInfo m_version;
	std::vector<line_t> m_data;

	bool m_unsavedChanges{false};

	std::optional<std::filesystem::path> m_path;

	template<VersionInfo>
	friend std::unique_ptr<File> open_file(ApplicationContext &c,
										   const nlohmann::json &j,
										   const VersionInfo &actualVersion);

	template<VersionInfo>
	friend nlohmann::json to_json(const File &f);

	File(ApplicationContext &c, legacy_data_t &&data); // Version <=0.1.1
	File(ApplicationContext &c, std::vector<line_t> data,
		 VersionInfo version); // Version 0.2.0

	id_util::id_t m_callbackId{id_util::invalid_id()};
	void draw_callback(bool newLine, const std::vector<DataEntry> &points);
	void init_callback();

public:
	explicit File(ApplicationContext &c);
	~File() override;

	bool save();
	bool save_as(std::filesystem::path p);
	[[nodiscard]] bool has_unsaved_changes() const;

	[[nodiscard]] bool has_path();
	[[nodiscard]] const wchar_t *path();

	[[nodiscard]] static bool can_open(const std::string &path);
	[[nodiscard]] static bool can_open(const std::filesystem::path &path);

	[[nodiscard]] static std::unique_ptr<File> open(ApplicationContext &c,
													const std::string &path);
	[[nodiscard]] static std::unique_ptr<File> open(
		ApplicationContext &c, const std::filesystem::path &path);

	bool set_path(std::filesystem::path p);

	std::vector<DataEntry>::size_type get_count();

	[[nodiscard]] const VersionInfo &get_version() const;

	[[nodiscard]] const std::vector<line_t> &get_all_lines() const;
};

} // namespace npp::file

#endif // NPP_FILE_HPP
