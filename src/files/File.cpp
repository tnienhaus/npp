//
// Created by Tobias on 1/25/2021.
//

#include <filesystem>
#include <fstream>

#include <nlohmann/json.hpp>
#include <utility>

#include <snappy.h>

#include "fileformat_helpers.hpp"

#include "../ApplicationContext.hpp"

#include "File.hpp"

namespace npp::file {

void to_json(nlohmann::json &j, const DataEntry &de) {
	using nlohmann::json;
	j = json{{"x", de.x}, {"y", de.y}, {"th", de.thickness}};
}

void from_json(const nlohmann::json &j, DataEntry &de) {
	using nlohmann::json;
	j.at("x").get_to(de.x);
	j.at("y").get_to(de.y);
	j.at("th").get_to(de.thickness);
}

DataEntry::DataEntry(const legacy::PointData &d) :
	x{d.x},
	y{d.y},
	thickness{d.pressure} {}

File::File(ApplicationContext &c) :
	ContextHolder(c),
	m_version{g_currentVersion},
	m_unsavedChanges{false} {
	init_callback();
}

void File::init_callback() {
	m_callbackId =
		get_context().register_draw_callback([=](auto b, const auto &points) {
			draw_callback(b, points);
		});
}

File::~File() {
	get_context().deregister_draw_callback(m_callbackId);
}

bool File::has_unsaved_changes() const {
	return m_unsavedChanges;
}

void File::draw_callback(bool newLine, const std::vector<DataEntry> &points) {
	if (m_data.empty() || (newLine && !m_data.back().empty())) {
		m_data.emplace_back();
		m_unsavedChanges = true;
	}
	if (points.empty()) { return; }
	m_data.back().reserve(m_data.back().size() + points.size());
	m_data.back().insert(m_data.back().end(), points.begin(), points.end());
	m_unsavedChanges = true;
}

bool File::has_path() {
	return m_path.has_value();
}

bool File::can_open(const std::string &p) {
	return can_open(std::filesystem::path{p});
}

bool File::can_open(const std::filesystem::path &path) {
	namespace fs = std::filesystem;
	if (!path.has_filename()) {
		return false;
	} else if (std::wstring ext = path.extension().c_str();
			   ext == L".bnpp" || ext == L".npp") {
		return true;
	}
	return false;
}

bool File::set_path(std::filesystem::path p) {
	if (can_open(p)) {
		m_path.emplace(std::move(p));
		return true;
	} else {
		return false;
	}
}

std::vector<DataEntry>::size_type File::get_count() {
	return m_data.size();
}

std::unique_ptr<File> File::open(ApplicationContext &c,
								 const std::string &path) {
	return open(c, std::filesystem::path{path});
}

template<VersionInfo v = VersionInfo{0, 1, 1}>
std::unique_ptr<File> open_file(ApplicationContext &c, const nlohmann::json &j,
								const VersionInfo &actualVersion) {
	if (j.contains("data")) {
		File::legacy_data_t data{};
		j.at("data").get_to(data);
		return std::unique_ptr<File>(new File(c, std::move(data)));
	} else {
		return nullptr;
	}
}

template<>
std::unique_ptr<File> open_file<VersionInfo(0, 2, 0)>(
	ApplicationContext &c, const nlohmann::json &j,
	const VersionInfo &actualVersion) {
	if (j.contains("data")) {
		std::vector<line_t> data{};
		j.at("data").get_to(data);
		return std::unique_ptr<File>(
			new File(c, std::move(data), actualVersion));
	} else {
		return nullptr;
	}
}

std::unique_ptr<File> File::open(ApplicationContext &c,
								 const std::filesystem::path &path) {
	if (!can_open(path)) {
		return nullptr;
	} else {
		auto js{file_to_json(path)};

		VersionInfo version{js.value("version", VersionInfo{0, 1, 1})};

		std::unique_ptr<File> file{nullptr};
		if (version <= VersionInfo{0, 1, 1}) {
			file = open_file<VersionInfo{0, 1, 1}>(c, js, version);
		} else if (version <= VersionInfo{0, 2, 0}) {
			file = open_file<VersionInfo{0, 2, 0}>(c, js, version);
		} else {
			// unknown version
			return nullptr;
		}
		if (file) { file->set_path(path); }
		return std::move(file);
	}
}

File::File(ApplicationContext &c, legacy_data_t &&data) :
	ContextHolder{c},
	m_version{0, 1, 1}, // <=v0.1.1 has no version info, so version doesn't
						// matter, as long as its <=v0.1.1
	m_unsavedChanges{false} {
	for (const auto &l : data) {
		m_data.emplace_back();
		auto &line{m_data.back()};
		for (const auto &p : l) {
			line.emplace_back(p.x, p.y, p.pressure * 2.5f);
		}
	}

	init_callback();
}

File::File(ApplicationContext &c, std::vector<line_t> data,
		   VersionInfo version) :
	ContextHolder{c},
	m_version{version}, // <=v0.1.1 has no version info, so version doesn't
						// matter, as long as its <=v0.1.1
	m_unsavedChanges{false},
	m_data{std::move(data)} {
	init_callback();
}

const VersionInfo &File::get_version() const {
	return m_version;
}

template<VersionInfo v = VersionInfo(0, 1, 1)>
nlohmann::json to_json(const File &f) {
	File::legacy_data_t data;
	// TODO reserve data size
	for (const auto &l : f.m_data) {
		data.emplace_back();
		auto &line{data.back()};
		for (const auto &p : l) {
			line.push({true, p.x, p.y, p.thickness / 2.5f});
		}
	}
	nlohmann::json js = nlohmann::json::object();
	js["data"] = data;
	return js;
}

template<>
nlohmann::json to_json<VersionInfo(0, 2, 0)>(const File &f) {
	//	assert(0); // not implemented
	nlohmann::json js = nlohmann::json::object();
	js["version"] = f.get_version();
	js["data"] = f.get_all_lines();
	return js;
}

bool File::save() {
	if (!m_path.has_value() || !can_open(m_path.value())) { return false; }
	auto js{nlohmann::json::object()};
	if (m_version <= VersionInfo{0, 1, 1}) {
		js = to_json<VersionInfo{0, 1, 1}>(*this);
	} else if (m_version <= VersionInfo{0, 2, 0}) {
		js = to_json<VersionInfo{0, 2, 0}>(*this);
	}
	if (json_to_file(js, m_path.value())) {
		m_unsavedChanges = false;
		return true;
	} else {
		return false;
	}
}

bool File::save_as(std::filesystem::path p) {
	auto oldPath{m_path};
	set_path(std::move(p));
	if (!save()) {
		m_path = oldPath;
		return false;
	} else {
		return true;
	}
}

const wchar_t *File::path() {
	if (m_path.has_value()) {
		return m_path->c_str();
	} else {
		return nullptr;
	}
}

const std::vector<line_t> &File::get_all_lines() const {
	return m_data;
}

} // namespace npp::file
