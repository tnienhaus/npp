//
// Created by Tobias on 1/26/2021.
//

#ifndef NPP_FILEFORMAT_HELPERS_HPP
#define NPP_FILEFORMAT_HELPERS_HPP

#include <filesystem>

#include <nlohmann/json_fwd.hpp>

namespace npp::file {

nlohmann::json file_to_json(const std::filesystem::path &pathToFile);

bool json_to_file(const nlohmann::json &j, const std::filesystem::path &path);

} // namespace npp::file

#endif // NPP_FILEFORMAT_HELPERS_HPP
