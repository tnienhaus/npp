//
// Created by Tobias on 1/12/2021.
//

#ifndef NPP_D2D_HPP
#define NPP_D2D_HPP

#include "win32.hpp"

#include <d2d1.h>
#pragma comment(lib, "d2d1")
#include <dwrite.h>
#pragma comment(lib, "dwrite")

#endif // NPP_D2D_HPP
