#include "aggregates/win32.hpp"

#include "util/string_utils.hpp"

#include "ApplicationContext.hpp"

#include "util/windows_helpers.hpp"

void weird_console_hack() {
	AllocConsole();
	FILE *fp;

	freopen_s(&fp, "CONOUT$", "w", stdout);
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
					LPWSTR lpCmdLine, int nCmdShow) {
	auto hr = CoInitializeEx(nullptr,
							 COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
	if (FAILED(hr)) {
		PostQuitMessage(-1);
		return -1;
	}

	npp::win::prepare_folders();

#ifdef DEBUG
	weird_console_hack();
#endif

	std::cout << "NPP build is " << NPP_BITNESS << " bit" << '\n';

	npp::ApplicationContext context{L"NPP", WS_OVERLAPPEDWINDOW};
	context.show(nCmdShow);

	if (wcscmp(lpCmdLine, L"") != 0) {
		auto path = npp::str_util::wstr_to_str(lpCmdLine);
		if(path.starts_with('"')) {
			path.erase(path.begin());
		}
		if(path.ends_with('"')) {
			path.pop_back();
		}
		if(npp::file::File::can_open(path)) {
			context.open_file(path);
		}
	}

	MSG msg{};
	while (GetMessage(&msg, nullptr, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	CoUninitialize();
	return 0;
}
