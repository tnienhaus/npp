//
// Created by Tobias on 1/25/2021.
//

#include <nlohmann/json.hpp>

#include "npp_version_info.hpp"

namespace npp {

using nlohmann::json;

void to_json(nlohmann::json &j, const VersionInfo &pd) {
	j = json{{"major", pd.major}, {"minor", pd.minor}, {"patch", pd.patch}};
}

void from_json(const nlohmann::json &j, VersionInfo &pd) {
	j.at("major").get_to(pd.major);
	j.at("minor").get_to(pd.minor);
	j.at("patch").get_to(pd.patch);
}

} // namespace npp
