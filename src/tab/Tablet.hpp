//
// Created by Tobias on 1/26/2021.
//

#ifndef NPP_TABLET_HPP
#define NPP_TABLET_HPP

#include <optional>
#include <queue>
#include <vector>

#include "../ContextHolder.hpp"

#include "../aggregates/win32.hpp"

#include "tablet_util.hpp"

namespace npp {

class Tablet : public ContextHolder {
public:
	struct PointData {
		bool valid{false};
		float x{};
		float y{};
		float pressure{};
	};

private:
	using point_data_t = PointData; // TODO weird
	using pointerid_t = tab_util::pointerid_t;

public:
	enum class Event { LEAVE, ENTER, DOWN, UP, UPDATE, UNHANDLED };

public:
	explicit Tablet(ApplicationContext &c);

	static constexpr UINT32 s_maxProperties = 32;

	struct Property {
		bool initialized;

		USHORT usagePageId;
		USHORT usageId;

		INT32 min;
		INT32 max;

		[[nodiscard]] float normalize(INT32 val,
									  bool shouldClamp = false) const;
	};

	Event handle_event(UINT msg, WPARAM wp);

	void pen_enter(pointerid_t id);
	void pen_exit(pointerid_t id);
	void pen_down(pointerid_t id);
	void pen_up(pointerid_t id);

	point_data_t get_next();
	std::vector<point_data_t> get_all();

	std::optional<point_data_t> get_pen_pos();

	std::queue<point_data_t>::size_type get_next_point_count();

	void update();

private:
	bool m_valid;
	bool m_down;
	pointerid_t m_pointer;
	// maybe use vector, if it has push (back) and pop (front)
	std::queue<point_data_t> m_points;
	point_data_t m_lastPenPos;
	bool m_penInFrame{};

	static constexpr Property s_pressureDefault = {true, 0x0, 0x0, 0, 1024};

	static constexpr Property s_tiltXDefault = {true, 0x0, 0x0, -9000, 9000};

	static constexpr Property s_tiltYDefault = {true, 0x0, 0x0, -9000, 9000};

	void read_properties();
	void clear_props();
	void set_properties_to_default();

	Property m_pressure;
	Property m_tiltX;
	Property m_tiltY;
};

} // namespace npp

#endif // NPP_TABLET_HPP
