//
// Created by Tobias on 12/14/2020.
//

#include "tablet_util.hpp"

namespace npp::tab_util {

PointerType id_to_type(pointerid_t id) {
	POINTER_INPUT_TYPE type;
	if (!GetPointerType(id, &type)) { return PointerType::INVALID; }
	return win32_ptype_to_lib(type);
}

} // namespace npp::tab_util
