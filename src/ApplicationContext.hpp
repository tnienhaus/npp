//
// Created by Tobias on 1/22/2021.
//

#ifndef NPP_APPLICATIONCONTEXT_HPP
#define NPP_APPLICATIONCONTEXT_HPP

#include <functional>
#include <memory>

#include "aggregates/win32.hpp"

#include "util/id_util.hpp"

#include "tab/Tablet.hpp"

#include "graphics/WindowHandler.hpp"
#include "graphics/d2d/D2DBitmapRenderer.hpp"
#include "graphics/d2d/TabletRenderer.hpp"

#include "ui/FileDialog.hpp"
#include "ui/UiHandler.hpp"

#include "files/File.hpp"

namespace npp {

class ApplicationContext {
public:
	static constexpr id_util::id_t s_callbackIdNum{0xCB};

private:
	using draw_callback_t =
		std::function<void(bool, const std::vector<file::DataEntry> &)>;
	std::map<id_util::id_t, draw_callback_t> m_drawCallbacks;

	std::unique_ptr<graphics::WindowHandler> m_windowHandler;
	std::unique_ptr<ui::UiHandler> m_uiHandler;
	std::unique_ptr<graphics::D2DBitmapRenderer<graphics::TabletRenderer>>
		m_renderer;

	std::unique_ptr<Tablet> m_tablet;
	std::unique_ptr<file::File> m_file;
	ui::FileDialog m_fileDialog;

	bool m_shouldStartNewLine;
	bool m_wantsToClose;
	bool m_fullRedrawThisFrame;

	void set_imgui_mouse_pos();
	bool handle_tablet_event(Tablet::Event event);

public:
	ApplicationContext(PCWSTR lpWindowName, DWORD dwStyle);
	LRESULT handle_events(UINT msg, WPARAM wp, LPARAM lp);

	void show(int cmdShow);

	[[nodiscard]] HWND get_window() const;

	[[nodiscard]] Tablet *get_tablet();

	[[nodiscard]] id_util::id_t register_draw_callback(
		const draw_callback_t &cb);
	bool deregister_draw_callback(id_util::id_t id);

	[[nodiscard]] const file::File *get_file() const;
	[[nodiscard]] file::File *get_file();

	[[nodiscard]] ui::FileDialog &get_file_dialog();

	[[nodiscard]] bool want_to_close() const;
	void cancel_close();

	[[nodiscard]] bool wants_full_redraw_this_frame() const;

	void open_file(const std::filesystem::path &path);
	void new_file();
};

} // namespace npp

#endif // NPP_APPLICATIONCONTEXT_HPP
