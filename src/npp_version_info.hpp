//
// Created by Tobias on 1/25/2021.
//

#ifndef NPP_NPP_VERSION_INFO_HPP
#define NPP_NPP_VERSION_INFO_HPP

#include <nlohmann/json_fwd.hpp>

#include <compare>

namespace npp {

struct VersionInfo {
	uint_fast8_t major;
	uint_fast8_t minor;
	uint_fast8_t patch;
	constexpr VersionInfo(uint_fast8_t major, uint_fast8_t minor,
						  uint_fast8_t patch) :
		major{major},
		minor{minor},
		patch{patch} {} // TODO test, test, test

	constexpr VersionInfo() : major{0}, minor{0}, patch{0} {}

	[[nodiscard]] constexpr uint_fast32_t shifted() const {
		return ((major << 16) | (minor << 8) | patch);
	}

	constexpr auto operator<=>(const VersionInfo &rhs) const = default;
};

constexpr VersionInfo g_currentVersion{NPP_VERSION_MAJOR, NPP_VERSION_MINOR,
									   NPP_VERSION_PATCH};

void to_json(nlohmann::json &j, const VersionInfo &pd);
void from_json(const nlohmann::json &j, VersionInfo &pd);

} // namespace npp

#endif // NPP_NPP_VERSION_INFO_HPP
