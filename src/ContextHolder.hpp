//
// Created by Tobias on 1/22/2021.
//

#ifndef NPP_CONTEXTHOLDER_HPP
#define NPP_CONTEXTHOLDER_HPP

namespace npp {

class ApplicationContext;

class ContextHolder {
private:
	ApplicationContext &m_context;

protected:
	ApplicationContext &get_context();

public:
	explicit ContextHolder(ApplicationContext &context);
	virtual ~ContextHolder() = default;
};

} // namespace npp

#endif // NPP_CONTEXTHOLDER_HPP
