//
// Created by Tobias on 1/22/2021.
//

#ifndef NPP_WINDOWHANDLER_HPP
#define NPP_WINDOWHANDLER_HPP

#include <map>

#include "../aggregates/d2d.hpp"
#include "../aggregates/d3d10.hpp"
#include "../aggregates/win32.hpp"

#include "../util/id_util.hpp"

#include "../ContextHolder.hpp"
#include "Win32Context.hpp"

namespace npp::graphics {

class WindowHandler :
	public ::npp::graphics::Win32Context<WindowHandler>,
	public ContextHolder {
private:
	CComPtr<ID3D10Device1> m_device;
	CComPtr<IDXGISwapChain> m_swapChain;
	CComPtr<ID2D1RenderTarget> m_d2dRenderTarget;
	CComPtr<ID3D10RenderTargetView> m_mainRenderTargetView;
	CComPtr<ID2D1Factory> m_d2dFactory;
	HRESULT create_swapchain();
	HRESULT create_device();
	HRESULT create_d2d_factory();

	bool m_recreateD2d;
	bool m_recreateD3d;

protected:
	[[nodiscard]] LPCWSTR class_name() const override;
	LRESULT handle_message(UINT msg, WPARAM wp, LPARAM lp) override;
	HRESULT on_valid_context_creation() override;

public:
	explicit WindowHandler(ApplicationContext &context);

	void clear();
	HRESULT draw();
	HRESULT on_resize(UINT w, UINT h);

	ID2D1RenderTarget *d2d_render_target();
	ID2D1Factory *d2d_factory();

	ID3D10RenderTargetView *d3d_render_target();
	ID3D10Device *d3d_device();

	HRESULT create_d2d_render_target();
	HRESULT create_d3d_render_target();
};

} // namespace npp::graphics

#endif // NPP_WINDOWHANDLER_HPP
