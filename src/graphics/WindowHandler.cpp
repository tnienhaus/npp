//
// Created by Tobias on 1/22/2021.
//

#include <imgui.h>

#include "../util/win32_helpers.hpp"

#include "WindowHandler.hpp"

#include "../ApplicationContext.hpp"

/// Return hr if FAILED(hr) is true
#define FAIL_RET(hr)                                                           \
	if (FAILED(hr)) return hr

namespace npp::graphics {

WindowHandler::WindowHandler(ApplicationContext &context) :
	Win32Context<WindowHandler>(), // maybe not necessary
	ContextHolder(context),
	m_recreateD2d{false},
	m_recreateD3d{false} {}

LPCWSTR WindowHandler::class_name() const {
	return L"NPP v0.2.0+ Window";
}

LRESULT WindowHandler::handle_message(UINT msg, WPARAM wp, LPARAM lp) {
	return get_context().handle_events(msg, wp, lp);
}

HRESULT WindowHandler::on_valid_context_creation() {
	HRESULT hr = create_device();

	FAIL_RET(hr);

	hr = create_swapchain();

	FAIL_RET(hr);

	hr = create_d2d_factory();

	FAIL_RET(hr);

	hr = create_d3d_render_target();

	FAIL_RET(hr);

	hr = create_d2d_render_target();

	return hr;
}

HRESULT WindowHandler::create_swapchain() {
	DXGI_SWAP_CHAIN_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.BufferCount = 1;
	desc.BufferDesc.Width = 0;
	desc.BufferDesc.Height = 0;
	desc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	// FIXME No fixed refresh rate
	//	desc.BufferDesc.RefreshRate.Numerator = 60;
	//	desc.BufferDesc.RefreshRate.Denominator = 1;
	desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	desc.OutputWindow = get_window();
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Windowed = TRUE;
	desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	IDXGIDevice1 *gxdiDevice;
	HRESULT hr = m_device->QueryInterface(&gxdiDevice);

	FAIL_RET(hr);

	IDXGIAdapter *gxdiAdapter;
	hr = gxdiDevice->GetAdapter(&gxdiAdapter);

	FAIL_RET(hr);

	IDXGIFactory *gxdiFactory;
	hr = gxdiAdapter->GetParent(IID_PPV_ARGS(&gxdiFactory));

	FAIL_RET(hr);

	hr = gxdiFactory->CreateSwapChain(m_device, &desc, &m_swapChain);

	return hr;
}

HRESULT WindowHandler::create_device() {
	UINT flags = 0;
#ifdef DEBUG
	flags |= D3D10_CREATE_DEVICE_DEBUG;
#endif
	flags |= D3D10_CREATE_DEVICE_BGRA_SUPPORT;

	static const D3D10_FEATURE_LEVEL1 levelAttempts[] = {
		D3D10_FEATURE_LEVEL_10_1, D3D10_FEATURE_LEVEL_10_0,
		D3D10_FEATURE_LEVEL_9_3,  D3D10_FEATURE_LEVEL_9_2,
		D3D10_FEATURE_LEVEL_9_1,
	};

	HRESULT hr;
	for (auto levelAttempt : levelAttempts) {
		ID3D10Device1 *device = nullptr;
		// TODO retry if Hardware not available
		hr = D3D10CreateDevice1(nullptr, D3D10_DRIVER_TYPE_HARDWARE, nullptr,
								flags, levelAttempt, D3D10_1_SDK_VERSION,
								&device);
		if (SUCCEEDED(hr)) {
			m_device = device;
			device = nullptr;
			break;
		}
	}

	return hr;
}

HRESULT WindowHandler::create_d2d_factory() {
	return D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_d2dFactory);
}

HRESULT WindowHandler::create_d2d_render_target() {
	if (m_d2dRenderTarget) {
		// TODO check if when resizing, this gets executed
		util::com_safe_release(m_d2dRenderTarget);
	}

	CComPtr<IDXGISurface> surface;
	// create_render_target

	auto dpi = GetDpiForWindow(get_window());

	D2D1_RENDER_TARGET_PROPERTIES props = D2D1::RenderTargetProperties(
		D2D1_RENDER_TARGET_TYPE_DEFAULT,
		D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED),
		dpi, dpi);

	auto hr = m_swapChain->GetBuffer(0, IID_PPV_ARGS(&surface));

	FAIL_RET(hr);

	hr = m_d2dFactory->CreateDxgiSurfaceRenderTarget(surface, props,
													 &m_d2dRenderTarget);

	return hr;
}

HRESULT WindowHandler::create_d3d_render_target() {
	if (m_mainRenderTargetView) {
		util::com_safe_release(m_mainRenderTargetView);
	}

	CComPtr<ID3D10Texture2D> backBuffer;
	auto hr = m_swapChain->GetBuffer(0, IID_PPV_ARGS(&backBuffer));

	FAIL_RET(hr);

	hr = m_device->CreateRenderTargetView(backBuffer, nullptr,
										  &m_mainRenderTargetView);

	FAIL_RET(hr);

	m_device->OMSetRenderTargets(1, &m_mainRenderTargetView.p, nullptr);

	return hr;
}

HRESULT WindowHandler::draw() {
	HRESULT hr{S_OK};
	if (m_recreateD2d) {
		hr = create_d2d_render_target();
		FAIL_RET(hr);
		m_recreateD2d = false;
	}
	if (m_recreateD3d) {
		hr = create_d3d_render_target();
		FAIL_RET(hr);
		m_recreateD3d = false;
	}

	hr = m_swapChain->Present(1, 0); // TODO select good values
	return hr;
}

HRESULT WindowHandler::on_resize(UINT w, UINT h) {
	if (m_mainRenderTargetView) {
		util::com_safe_release(m_mainRenderTargetView);
	}
	if (m_d2dRenderTarget) { util::com_safe_release(m_d2dRenderTarget); }

	HRESULT hr = m_swapChain->ResizeBuffers(0, w, h, DXGI_FORMAT_UNKNOWN, 0);
	FAIL_RET(hr);

	// First D3D, because D2D renders to it
	hr = create_d3d_render_target();

	FAIL_RET(hr);

	hr = create_d2d_render_target();

	return hr;
}

void WindowHandler::clear() {
	// TODO variable clear color, etc.
	constexpr float col[4]{0.f, 0.f, 0.f, 0.f};
	m_device->ClearRenderTargetView(m_mainRenderTargetView, col);
}

ID2D1RenderTarget *WindowHandler::d2d_render_target() {
	return m_d2dRenderTarget;
}

ID2D1Factory *WindowHandler::d2d_factory() {
	return m_d2dFactory;
}

ID3D10RenderTargetView *WindowHandler::d3d_render_target() {
	return m_mainRenderTargetView;
}

ID3D10Device *WindowHandler::d3d_device() {
	return m_device;
}

} // namespace npp::graphics

#undef FAIL_RET
