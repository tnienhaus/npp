//
// Created by Tobias on 1/25/2021.
//

#include <iostream>

#include "D2DRenderer.hpp"

#include "../../util/win32_helpers.hpp"

#ifdef DEBUG
static void error_description(HRESULT hr) {
	if (FACILITY_WINDOWS == HRESULT_FACILITY(hr)) hr = HRESULT_CODE(hr);
	TCHAR *szErrMsg;

	if (FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL,
			hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&szErrMsg, 0,
			nullptr) != 0) {
		_tprintf(TEXT("%s"), szErrMsg);
		LocalFree(szErrMsg);
	} else
		_tprintf(TEXT("[Could not find a description for error # %#x.]\n"), hr);
}
#else
static void error_description(HRESULT) {}
#endif

namespace npp::graphics {

HRESULT D2DRenderer::create_device_dependent_resources() {
	return S_OK;
}

void D2DRenderer::discard_device_dependent_resources() {}

HRESULT D2DRenderer::create_device_independent_resources() {
	auto hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_factory);
	return hr;
}

void D2DRenderer::discard_device_independent_resources() {
	util::com_safe_release(m_factory);
}

BOOL D2DRenderer::draw() {
	HRESULT hr{S_OK};
	m_renderTarget->BeginDraw();

	paint_event();

	hr = m_renderTarget->EndDraw();
	if (FAILED(hr) || hr == D2DERR_RECREATE_TARGET) {
		discard_device_dependent_resources();
	}
	return SUCCEEDED(hr);
}

void D2DRenderer::on_resize(UINT, UINT) {
	discard_device_dependent_resources();
	create_device_dependent_resources();
}

ID2D1Factory *D2DRenderer::get_factory() {
	return m_factory;
}

D2DRenderer::D2DRenderer(ApplicationContext &c) :
	ContextHolder(c),
	m_renderTarget{nullptr} {}

HRESULT D2DRenderer::initialize() {
	auto hr{create_device_independent_resources()};
	return hr;
}

ID2D1RenderTarget *D2DRenderer::get_render_target() {
	return m_renderTarget;
}

void D2DRenderer::set_render_target(ID2D1RenderTarget *target) {
	discard_device_dependent_resources();
	m_renderTarget = target;
	create_device_dependent_resources();
}

} // namespace npp::graphics
