//
// Created by Tobias on 1/25/2021.
//

#include "TabletRenderer.hpp"

#include "../../ApplicationContext.hpp"

namespace npp::graphics {

void TabletRenderer::paint_event() {
	if (get_context().wants_full_redraw_this_frame()) {
		get_render_target()->SetTransform(D2D1::Matrix3x2F::Identity());

		full_redraw();
	}
}

HRESULT TabletRenderer::create_device_dependent_resources() {
	auto hr = D2DRenderer::create_device_dependent_resources();
	if (SUCCEEDED(hr)) {
		constexpr D2D1_COLOR_F col{1.0f, 1.0f, 1.0f, 1.0f};
		hr = get_render_target()->CreateSolidColorBrush(col, &m_brush);
	}
	return hr;
}

void TabletRenderer::discard_device_dependent_resources() {
	util::com_safe_release(m_brush);
	D2DRenderer::discard_device_dependent_resources();
}

TabletRenderer::TabletRenderer(ApplicationContext &c) :
	D2DRenderer{c},
	m_lastPoint{},
	m_brush{nullptr} {
	m_callbackId =
		c.register_draw_callback([=](auto newLine, const auto &points) {
			draw_points(newLine, points);
		});
}

TabletRenderer::~TabletRenderer() {
	get_context().deregister_draw_callback(m_callbackId);
}

void TabletRenderer::draw_points(bool newLine,
								 const std::vector<file::DataEntry> &points) {
	get_render_target()->BeginDraw();
	// new data
	if (newLine) { m_lastPoint = points[0]; }
	for (int i = newLine ? 1 : 0; i < points.size(); ++i) {
		const auto &next{points[i]};
		auto a = D2D1::Point2F(m_lastPoint.x, m_lastPoint.y);
		auto b = D2D1::Point2F(next.x, next.y);
		get_render_target()->DrawLine(a, b, m_brush, next.thickness, nullptr);
		m_lastPoint = next;
	}
	get_render_target()->EndDraw(); // TODO handle errors -> recreate target
}

void TabletRenderer::full_redraw() {
	constexpr D2D1_COLOR_F clearCol{0.f, 0.f, 0.f, 0.f};
	get_render_target()->Clear(clearCol);
	for (const auto &line : get_context().get_file()->get_all_lines()) {
		file::DataEntry lastPoint{};
		bool skipFirst{true};
		for (const auto &p : line) {
			if (skipFirst) {
				skipFirst = false;
			} else {
				auto a = D2D1::Point2F(lastPoint.x, lastPoint.y);
				auto b = D2D1::Point2F(p.x, p.y);
				get_render_target()->DrawLine(a, b, m_brush, p.thickness,
											  nullptr);
			}
			lastPoint = p;
		}
	}
}

} // namespace npp::graphics
