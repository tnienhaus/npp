//
// Created by Tobias on 1/25/2021.
//

#ifndef NPP_D2DRENDERER_HPP
#define NPP_D2DRENDERER_HPP

#include "../../aggregates/d2d.hpp"
#include "../../aggregates/win32.hpp"

#include "../../ContextHolder.hpp"

namespace npp::graphics {

class D2DRenderer : public ContextHolder {
private:
	ID2D1RenderTarget *m_renderTarget;
	CComPtr<ID2D1Factory> m_factory;

protected:
	virtual void paint_event() = 0;

	virtual HRESULT create_device_dependent_resources();
	virtual void discard_device_dependent_resources();
	virtual HRESULT create_device_independent_resources();
	virtual void discard_device_independent_resources();

	ID2D1Factory *get_factory();
	ID2D1RenderTarget *get_render_target();

public:
	explicit D2DRenderer(ApplicationContext &c);
	~D2DRenderer() override = default;
	BOOL draw();

	void set_render_target(ID2D1RenderTarget *target);
	void on_resize(UINT, UINT);
	HRESULT initialize();
};

} // namespace npp::graphics

#endif // NPP_D2DRENDERER_HPP
