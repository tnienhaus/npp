//
// Created by Tobias on 1/25/2021.
//

#ifndef NPP_D2DBITMAPRENDERER_HPP
#define NPP_D2DBITMAPRENDERER_HPP

#include <concepts>

#include "../../util/win32_helpers.hpp"

#include "D2DRenderer.hpp"

namespace npp::graphics {

template<typename renderer_t>
requires std::is_base_of_v<D2DRenderer, renderer_t> class D2DBitmapRenderer :
	public D2DRenderer {
private:
	renderer_t m_renderer;
	CComPtr<ID2D1BitmapRenderTarget> m_bitmapTarget;

	HRESULT create_bitmap_target() {
		util::com_safe_release(m_bitmapTarget);

		static const D2D1_PIXEL_FORMAT format = D2D1::PixelFormat(
			DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED);

		auto hr = get_render_target()->CreateCompatibleRenderTarget(
			nullptr, nullptr, &format,
			D2D1_COMPATIBLE_RENDER_TARGET_OPTIONS_NONE, &m_bitmapTarget);

		if (FAILED(hr)) { return hr; }

		m_renderer.set_render_target(m_bitmapTarget);

		return hr;
	}

protected:
	HRESULT create_device_dependent_resources() override {
		auto hr = D2DRenderer::create_device_dependent_resources();

		if (FAILED(hr)) { return hr; }

		hr = create_bitmap_target();

		return hr;
	}

	void discard_device_dependent_resources() override {
		util::com_safe_release(m_bitmapTarget);
		D2DRenderer::discard_device_dependent_resources();
	}

	void paint_event() override {
		if (m_renderer.draw()) {
			ID2D1Bitmap *bitmap;
			auto hr = m_bitmapTarget->GetBitmap(&bitmap);
			if (SUCCEEDED(hr)) {
				auto destSize{get_render_target()->GetSize()};
				D2D1_RECT_F destRect =
					D2D1::RectF(0.f, 0.f, destSize.width, destSize.height);
				auto sourceSize{bitmap->GetSize()};
				D2D1_RECT_F sourceRect =
					D2D1::RectF(0.f, 0.f, sourceSize.width, sourceSize.height);
				get_render_target()->DrawBitmap(
					bitmap, &destRect, 1.f,
					D2D1_BITMAP_INTERPOLATION_MODE_LINEAR, &sourceRect);
			}
			//			get_render_target().Draw
		} else {
			// TODO somehow recreate a new bitmap render target
			create_bitmap_target();
		}
	}

public:
	explicit D2DBitmapRenderer(ApplicationContext &c) :
		D2DRenderer{c},
		m_renderer{c} {}
};

} // namespace npp::graphics

#endif // NPP_D2DBITMAPRENDERER_HPP
