//
// Created by Tobias on 1/25/2021.
//

#ifndef NPP_TABLETRENDERER_HPP
#define NPP_TABLETRENDERER_HPP

#include "../../files/File.hpp"

#include "D2DRenderer.hpp"

namespace npp::graphics {

class TabletRenderer : public D2DRenderer {
private:
	file::DataEntry m_lastPoint;

	CComPtr<ID2D1SolidColorBrush> m_brush;

	id_util::id_t m_lastPacketId{id_util::invalid_id()};
	id_util::id_t m_callbackId{id_util::invalid_id()};

protected:
	void paint_event() override;

	HRESULT create_device_dependent_resources() override;
	void discard_device_dependent_resources() override;

	void draw_points(bool newLine, const std::vector<file::DataEntry> &points);
	void full_redraw();

public:
	explicit TabletRenderer(ApplicationContext &c);
	~TabletRenderer() override;
};

} // namespace npp::graphics

#endif // NPP_TABLETRENDERER_HPP
