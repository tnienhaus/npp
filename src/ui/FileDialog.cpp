//
// Created by Tobias on 1/26/2021.
//

#include "FileDialog.hpp"

#include "../ApplicationContext.hpp"

#include "../util/windows_helpers.hpp"

namespace npp::ui {

FileDialog::FileDialog(ApplicationContext &c) :
	ContextHolder(c),
	m_mode{OpenMode::NONE} {}

void FileDialog::open(FileDialog::OpenMode mode) {
	namespace fs = std::filesystem;
	static std::string titleOpen{"Open File"};
	static std::string titleSave{"Save As"};

	m_mode = mode;

	ImGuiFileDialogFlags flags = ImGuiFileDialogFlags_None;
	if (m_mode != OpenMode::OPEN) {
		flags |= ImGuiFileDialogFlags_ConfirmOverwrite;
	}

	if (get_context().get_file()->has_path()) {
		fs::path p{get_context().get_file()->path()};
		m_fileDialog.OpenModal(s_dialogName,
							   m_mode == OpenMode::OPEN ? titleOpen : titleSave,
							   s_filters, p.parent_path().string(),
							   p.filename().string(), 1, nullptr, flags);
	} else {
		m_fileDialog.OpenModal(s_dialogName,
							   m_mode == OpenMode::OPEN ? titleOpen : titleSave,
							   s_filters, win::get_default_path(), "notes.bnpp",
							   1, nullptr, flags);
	}
}

bool FileDialog::is_open() {
	return m_fileDialog.IsOpened(s_dialogName);
}

void FileDialog::display() {
	bool reopen = false;
	if (m_fileDialog.Display(s_dialogName)) {
		if (m_fileDialog.IsOk()) {
			switch (m_mode) {
			case OpenMode::SAVE_AS:
				get_context().get_file()->save_as(
					m_fileDialog.GetFilePathName());
				m_mode = OpenMode::NONE;
				break;
			case OpenMode::OPEN:
				get_context().open_file(m_fileDialog.GetFilePathName());
				m_mode = OpenMode::NONE;
				break;
			case OpenMode::SAVE_OPEN:
				get_context().get_file()->save_as(
					m_fileDialog.GetFilePathName());
				reopen = true;
				break;
			case OpenMode::SAVE_AS_THEN_CLEAR:
				get_context().get_file()->save_as(
					m_fileDialog.GetFilePathName());
				get_context().new_file();
				m_mode = OpenMode::NONE;
				break;
			case OpenMode::NONE:
			default:
				break;
			}
		}
		m_fileDialog.Close();
	}
	if (reopen) { open(OpenMode::OPEN); }
}

} // namespace npp::ui
