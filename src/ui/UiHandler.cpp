//
// Created by Tobias on 1/25/2021.
//

#include "../ApplicationContext.hpp"

#include "UiHandler.hpp"

#include <imgui.h>
#include <imgui_impl_dx10.h>
#include <imgui_impl_win32.h>

IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg,
													  WPARAM wParam,
													  LPARAM lParam);

namespace npp::ui {

UiHandler::Frame::Frame() {
	ImGui_ImplDX10_NewFrame();
	ImGui_ImplWin32_NewFrame();
}

UiHandler::Frame::~Frame() {
	ImGui::Render();
	auto *data = ImGui::GetDrawData();
	ImGui_ImplDX10_RenderDrawData(data);
}

UiHandler::UiHandler(ApplicationContext &context, HWND wind, ID3D10Device *dev,
					 ID3D10RenderTargetView *renderTargetView) :
	ContextHolder{context},
	m_renderTargetView{renderTargetView},
	m_nextMode{ReplacementMode::NONE} {
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO &io = ImGui::GetIO();
	(void)io;
	ImGui::StyleColorsDark();
	ImGui_ImplWin32_Init(wind);
	ImGui_ImplDX10_Init(dev);
	ImGui_ImplWin32_EnableDpiAwareness();
}

void UiHandler::frame() {
	ImGui::NewFrame();

	constexpr const char *modalId = "Unsaved Changes";
	bool openModal = false;

	ImGui::BeginMainMenuBar();
	if (ImGui::BeginMenu("File")) {
		if (ImGui::MenuItem("New")) {
			if (get_context().get_file()->has_unsaved_changes()) {
				m_nextMode = ReplacementMode::NEW;
				openModal = true;
			} else {
				get_context().new_file();
			}
		}
		ImGui::Separator();
		if (ImGui::MenuItem("Open")) {
			if (get_context().get_file()->has_unsaved_changes()) {
				std::cout << "Unsaved Changes!\n";
				m_nextMode = ReplacementMode::OPEN;
				openModal = true;
			} else {
				get_context().get_file_dialog().open(
					FileDialog::OpenMode::OPEN);
			}
		}
		ImGui::Separator();
		if (ImGui::MenuItem("Save")) {
			if (get_context().get_file()->has_path()) {
				get_context().get_file()->save();
			} else {
				get_context().get_file_dialog().open(
					FileDialog::OpenMode::SAVE_AS);
			}
		}
		if (ImGui::MenuItem("Save as")) {
			get_context().get_file_dialog().open(FileDialog::OpenMode::SAVE_AS);
		}
		ImGui::EndMenu();
	}
	ImGui::EndMainMenuBar();

	if (openModal && !ImGui::IsPopupOpen(modalId)) {
		ImGui::OpenPopup(modalId);
	}

	if (ImGui::BeginPopupModal(modalId, nullptr,
							   ImGuiWindowFlags_AlwaysAutoResize)) {
		if (m_nextMode != ReplacementMode::NONE) {
			ImGui::Text("You have unsaved changes! Discard?");
			ImGui::Separator();
			if (ImGui::Button("Discard")) {
				if (m_nextMode == ReplacementMode::OPEN) {
					get_context().get_file_dialog().open(
						FileDialog::OpenMode::OPEN);
				} else if (m_nextMode == ReplacementMode::NEW) {
					get_context().new_file();
				}
				ImGui::CloseCurrentPopup();
				m_nextMode = ReplacementMode::NONE;
			}
			ImGui::SetItemDefaultFocus();
			ImGui::SameLine();
			if (ImGui::Button("Save Changes")) {
				if (get_context().get_file()->has_path()) {
					get_context().get_file()->save();
					if (m_nextMode == ReplacementMode::OPEN) {
						get_context().get_file_dialog().open(
							FileDialog::OpenMode::OPEN);
					} else if (m_nextMode == ReplacementMode::NEW) {
						get_context().new_file();
					}
				} else {
					if (m_nextMode == ReplacementMode::OPEN) {
						get_context().get_file_dialog().open(
							FileDialog::OpenMode::SAVE_OPEN);
					} else if (m_nextMode == ReplacementMode::NEW) {
						get_context().get_file_dialog().open(
							FileDialog::OpenMode::SAVE_AS_THEN_CLEAR);
					}
				}
				ImGui::CloseCurrentPopup();
				m_nextMode = ReplacementMode::NONE;
			}
			ImGui::SameLine();
			if (ImGui::Button("Cancel")) {
				ImGui::CloseCurrentPopup();
				m_nextMode = ReplacementMode::NONE;
			}
		}
		ImGui::EndPopup();
	}

	if (get_context().want_to_close() && !ImGui::IsPopupOpen("Close?")) {
		ImGui::OpenPopup("Close?");
	}

	if (ImGui::BeginPopupModal("Close?", nullptr,
							   ImGuiWindowFlags_AlwaysAutoResize)) {
		ImGui::Text("You have unsaved changes.");
		if (ImGui::Button("Yes")) {
			DestroyWindow(get_context().get_window());
			get_context().cancel_close();
			ImGui::CloseCurrentPopup();
		}
		ImGui::SameLine();
		ImGui::SetItemDefaultFocus();
		if (ImGui::Button("No")) {
			get_context().cancel_close();
			ImGui::CloseCurrentPopup();
		}
		ImGui::EndPopup();
	}
}

LRESULT UiHandler::handle_message(HWND wind, UINT msg, WPARAM wp, LPARAM lp) {
	if (ImGui_ImplWin32_WndProcHandler(wind, msg, wp, lp)) { return TRUE; }
	return 0;
}

void UiHandler::set_render_target(ID3D10RenderTargetView *target) {
	m_renderTargetView = target;
}

} // namespace npp::ui
