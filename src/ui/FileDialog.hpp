//
// Created by Tobias on 1/26/2021.
//

#ifndef NPP_FILEDIALOG_HPP
#define NPP_FILEDIALOG_HPP

#include <ImGuiFileDialog.h>

#include "../ContextHolder.hpp"

namespace npp::ui {

class FileDialog : public ContextHolder {
public:
	enum class OpenMode {
		NONE = -1,
		SAVE_AS,
		OPEN,
		SAVE_OPEN,
		SAVE_AS_THEN_CLEAR
	};

private:
	static constexpr auto s_dialogName{"DIALOG"};
	static constexpr const char *s_filters =
		"Compressed (*.bnpp){.bnpp}, Plain (*.npp){.npp}, All (*.bnpp; "
		"*.npp){.bnpp,.npp}";

	ImGuiFileDialog m_fileDialog;

	OpenMode m_mode;

public:
	explicit FileDialog(ApplicationContext &c);

	void open(OpenMode mode);
	[[nodiscard]] bool is_open();

	void display();
};

} // namespace npp::ui

#endif // NPP_FILEDIALOG_HPP
