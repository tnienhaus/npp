//
// Created by Tobias on 1/25/2021.
//

#ifndef NPP_UIHANDLER_HPP
#define NPP_UIHANDLER_HPP

#include "../aggregates/d3d10.hpp"
#include "../aggregates/win32.hpp"

#include "../ContextHolder.hpp"

namespace npp::ui {

class UiHandler : public ContextHolder {
private:
	enum class ReplacementMode { NEW, OPEN, NONE };

public:
	struct Frame {
		Frame();
		~Frame();
	};

private:
	ID3D10RenderTargetView *m_renderTargetView;
	ReplacementMode m_nextMode;

public:
	UiHandler(ApplicationContext &context, HWND wind, ID3D10Device *dev,
			  ID3D10RenderTargetView *renderTargetView);

	void frame();
	static LRESULT handle_message(HWND wind, UINT msg, WPARAM wp, LPARAM lp);
	void set_render_target(ID3D10RenderTargetView *target);
};

} // namespace npp::ui

#endif // NPP_UIHANDLER_HPP
