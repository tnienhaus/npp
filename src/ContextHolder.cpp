//
// Created by Tobias on 1/22/2021.
//

#include "ContextHolder.hpp"

namespace npp {

ApplicationContext &ContextHolder::get_context() {
	return m_context;
}

ContextHolder::ContextHolder(ApplicationContext &context) :
	m_context{context} {}

} // namespace npp
