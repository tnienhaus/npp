# npp (Notes++)

[![Download tnienhaus-npp](https://img.shields.io/sourceforge/dt/tnienhaus-npp.svg)](https://sourceforge.net/projects/tnienhaus-npp/files/latest/download)  
[Website](https://www.tobiasnienhaus.me/projects/npp/page.html)

This project was part of a course where we could define our own learning goals, essentially giving us great freedom in what we would do.

I chose to do some sort of note taking application for pen tablets (the kind you plug into your PC), as I found there was not really a great selection of ones (at least I couldn't find them). A goal of mine was to utilize new language features (C++20), as well as look into APIs for pen tablets. After some thought, I landed on the Windows Pointer API, introduced in Windows 8.

Initially I wanted to use OpenGL (and GLFW) for this, as I am familiar with both. This turned out to not work with the event based Windows API. In the end, I chose to use Win32, which lead to Direct2D. As a UI I chose to use ImGui, which lead to me using a weird marriage of Direct3D and Direct2D, as ImGui needs Direct3D to work.

### Features

- Draw with your tablet
  - Supports at least Wacom One Small (tested with it), but should support everything supporting the Windows Pointer API
- Save to and load from file
  - Filetype: .npp
    - This format saves all the information in plain JSON
  - Filetype: .bnpp
    - This format saves all the information in BSON (Binary JSON), while compressing it afterwards using Google's Snappy
- Open files by dragging them onto the exe and via "Open with"
- UI using ImGui

### Used libraries

- [nlohmann::json](https://github.com/nlohmann/json)
  - This library is used for reading and writing data from and to JSON and BSON.
- [snappy](https://github.com/google/snappy)
  - This library is used for compressing and decrompessing data. Snappy is more geared towards speed than maximum compression, but still offers a great amount of compression.
- [ImGui](https://github.com/ocornut/imgui)
  - This library is used for UI. It is fantastic. Use it.
- [ImGuiFileBrowser](https://github.com/aiekick/ImGuiFileDialog)
  - More of an extension to ImGui, this library offers a file dialog modal, as the ones offered by Windows don't seem to work with Direct3D and/or Direct2D.
- [magic_enum](https://github.com/Neargye/magic_enum)
  - This library actually doesn't serve that much purpose in this context. It has amazing features, however, I only use it for getting the names of enums without a million switch cases.

### Downloading

Because GitLab doesn't offer release hosting (at least an easy way of doing so), I host all the releases on [SourceForge](https://sourceforge.net/projects/tnienhaus-npp/).

---

This project previously had its home on [GitHub](https://github.com/TobiasNienhaus/npp). There will be no more changes on GitHub and everything new will be added here.
